import 'package:flutter/cupertino.dart';
import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:easy_rich_text/easy_rich_text.dart';
import 'package:weather/weather.dart';
import 'package:intl/intl.dart';

String? tempMin = "";
String? tempMax = "";
String? temp = "";
String? detailedTemp = "";
String? weatherMain = "";
String? weatherIcon = "";
String? iconName = "";

List<Weather> forecastWeather = [];
var formatter = new DateFormat.yMEd();
var formatter2 = new DateFormat.Md();
var formatter3 = new DateFormat.EEEE();
var formatter4 = new DateFormat.Hms();
String? formattedDate;
String? formattedTime;

final String cityName = "Dubai";

class Day extends StatefulWidget {
  final String id = "day";
  List<Weather>? dayWeather;

  Day({this.dayWeather});
  @override
  _DayState createState() => _DayState();
}

class _DayState extends State<Day> {
  @override
  void initState() {
    print("SecondScreenWeatherArray");
    print(widget.dayWeather![0]);
    print(widget.dayWeather![1]);
    print(widget.dayWeather![2]);
    print(widget.dayWeather![3]);
    print(widget.dayWeather![4]);
    print(widget.dayWeather![5]);
    print(widget.dayWeather![6]);
    print(widget.dayWeather![7]);
    setState(() {
      tempMin = (widget.dayWeather![0].tempMin!.celsius)!.round().toString();
      tempMax =
          (widget.dayWeather![0].tempMax!.celsius)!.round().toString() + "/";
      temp = (widget.dayWeather![0].temperature!.celsius)!.round().toString();
      weatherMain = widget.dayWeather![0].weatherMain;
      weatherIcon = widget.dayWeather![0].weatherIcon;
      formattedDate = formatter.format(widget.dayWeather![0].date!);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Elias Dahi Weather App"),
          centerTitle: true,
          backgroundColor: Color(0xFFBF360C),
        ),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.fromLTRB(
                    width * 0.05, height * 0.02, width * 0.05, height * 0.03),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "${cityName}",
                      style: TextStyle(fontSize: width * height * 0.0001),
                    ),
                    Text(
                      "${formattedDate}",
                      style: TextStyle(fontSize: width * height * 0.0001),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(
                    width * 0.05, 0.0, width * 0.05, height * 0.03),
                child: Row(
                  children: [
                    Text(
                      "${weatherMain}",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: width * height * 0.0001),
                    ),
                    Spacer(),
                    Text(
                      "${tempMax}${tempMin}",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: width * height * 0.0001),
                    ),
                    EasyRichText(
                      " \u00B0" + "C",
                      patternList: [
                        EasyRichTextPattern(
                          targetString: "C",
                          superScript: true,
                          stringBeforeTarget: ' \u00B0',
                          matchWordBoundaries: false,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(
                  width * 0.05,
                  0.0,
                  width * 0.05,
                  height * 0.0049,
                ),
                child: Row(
                  children: [
                    Image.network(
                        "https://openweathermap.org/img/wn/${weatherIcon}@2x.png"),
                    Spacer(),
                    Text(
                      "${temp}",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: width * height * 0.0005),
                    ),
                    EasyRichText(
                      "\u00B0C",
                      textScaleFactor: 3,
                      patternList: [
                        EasyRichTextPattern(
                          targetString: "\u00B0C",
                          superScript: true,
                          stringBeforeTarget: '',
                          matchWordBoundaries: false,
                          subScript: false,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(
                  width * 0.09,
                  0.0,
                  width * 0.05,
                  0.0,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      "Detailed Day Forecast",
                      style: TextStyle(fontSize: width * height * 0.0001),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: height * 0.5,
                child: ListView.builder(
                    itemCount: widget.dayWeather!.length,
                    scrollDirection: Axis.vertical,
                    itemBuilder: (context, index) {
                      return ListTile(
                        leading: Image.network(
                            "https://openweathermap.org/img/wn/${widget.dayWeather![index].weatherIcon}.png"),
                        title: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text(
                                "${formatter4.format(widget.dayWeather![index].date!)}"),
                            Text(
                              ".",
                              style: TextStyle(
                                  fontSize: height * width * 0.0002,
                                  fontWeight: FontWeight.bold,
                                  height: 0.3),
                            ),
                            Text(
                                "${widget.dayWeather![index].weatherDescription}"),
                          ],
                        ),
                        trailing: EasyRichText(
                          "${(widget.dayWeather![index].temperature!.celsius)!.round().toString()} \u00B0" +
                              "C",
                          patternList: [
                            EasyRichTextPattern(
                              targetString: "C",
                              superScript: true,
                              stringBeforeTarget: ' \u00B0',
                              matchWordBoundaries: false,
                            ),
                          ],
                        ),
                      );
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
