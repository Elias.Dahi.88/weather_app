import 'dart:io';
import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_rich_text/easy_rich_text.dart';
import 'package:weather/weather.dart';
import 'package:intl/intl.dart';
import 'package:weather_app/pages/day.dart';

WeatherFactory wf = new WeatherFactory("b72dbe4e388be4657c1346c599dd2c3e",
    language: Language.ENGLISH);

String? tempMin = "";
String? tempMax = "";
String? temp = "";
String? weatherMain = "";
String? weatherIcon = "";
List<Weather> forecastWeather = [];
List<Weather> Allforecast = [];
List<Weather>? secondScreenWeather = [];
bool Loading = true;
String? iconName = "";
var now = new DateTime.now();
var formatter = new DateFormat.yMEd();
var formatter2 = new DateFormat.Md();
var formatter3 = new DateFormat.EEEE();
var formatter4 = new DateFormat.Hms();
String formattedDate = formatter.format(now);

final String cityName = "Dubai";

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  final String id = "home";

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  GetCurrentWeather() async {
    Weather weather = await wf.currentWeatherByCityName(cityName);
    List<Weather> forecast = await wf.fiveDayForecastByCityName(cityName);
    setState(() {
      Allforecast = forecast;
      tempMin = (weather.tempMin!.celsius)!.round().toString();
      tempMax = (weather.tempMax!.celsius)!.round().toString() + "/";
      temp = (weather.temperature!.celsius)!.round().toString();
      weatherMain = weather.weatherMain;
      weatherIcon = weather.weatherIcon;
      List<Weather> tempwth = [];
      for (int i = 0; i < forecast.length; i += 8) {
        tempwth.add(forecast[i]);
        print("from forecast looping");
        print(forecast[i].date);
      }
      forecastWeather = tempwth;
      Loading = false;
    });
    print("forecastWeather.length");
    print(forecastWeather.length);
  }

  @override
  void initState() {
    GetCurrentWeather();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    if (Loading == true) {
      if (Platform.isIOS) {
        return Container(
          color: Colors.white,
          child: Center(
            child: CupertinoActivityIndicator(
              radius: 20.0,
            ),
          ),
        );
      }
      if (Platform.isAndroid) {
        return Container(
          color: Colors.white,
          child: Center(
            child: CircularProgressIndicator(),
          ),
        );
      }
    }
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Elias Dahi Weather App"),
          centerTitle: true,
          backgroundColor: Color(0xFFBF360C),
        ),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.fromLTRB(
                    width * 0.05, height * 0.02, width * 0.05, height * 0.03),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "${cityName}",
                      style: TextStyle(fontSize: width * height * 0.0001),
                    ),
                    Text(
                      "${formattedDate}",
                      style: TextStyle(fontSize: width * height * 0.0001),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(
                    width * 0.05, 0.0, width * 0.05, height * 0.03),
                child: Row(
                  children: [
                    Text(
                      "${weatherMain}",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: width * height * 0.0001),
                    ),
                    Spacer(),
                    Text(
                      "${tempMax}${tempMin}",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: width * height * 0.0001),
                    ),
                    EasyRichText(
                      " \u00B0" + "C",
                      patternList: [
                        EasyRichTextPattern(
                          targetString: "C",
                          superScript: true,
                          stringBeforeTarget: ' \u00B0',
                          matchWordBoundaries: false,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(
                  width * 0.05,
                  0.0,
                  width * 0.05,
                  height * 0.0007,
                ),
                child: Row(
                  children: [
                    Image.network(
                        "https://openweathermap.org/img/wn/${weatherIcon}@2x.png"),
                    Spacer(),
                    Text(
                      "${temp}",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: width * height * 0.0005),
                    ),
                    EasyRichText(
                      "\u00B0C",
                      textScaleFactor: 3,
                      patternList: [
                        EasyRichTextPattern(
                          targetString: "\u00B0C",
                          superScript: true,
                          stringBeforeTarget: '',
                          matchWordBoundaries: false,
                          subScript: false,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(
                  width * 0.09,
                  0.0,
                  width * 0.05,
                  0.0,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      "5 - Day Forecast",
                      style: TextStyle(fontSize: width * height * 0.0001),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: height * 0.9,
                child: ListView.builder(
                  padding: EdgeInsets.symmetric(vertical: height * 0.015),
                  itemCount: forecastWeather.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {
                        setState(() {
                          secondScreenWeather = [];
                        });
                        for (int i = 0; i < 8; i++) {
                          print("i" + i.toString());
                          print("index" + index.toString());
                          setState(() {
                            secondScreenWeather!
                                .add(Allforecast[i + (8 * index)]);
                          });
                        }
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    Day(dayWeather: secondScreenWeather)));
                      },
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(
                          width * 0.03,
                          0.0,
                          width * 0.03,
                          0.0,
                        ),
                        child: Container(
                          width: width * 0.25,
                          alignment: AlignmentDirectional.center,
                          decoration: BoxDecoration(
                            color: Color(0xFFF3F0F0),
                            borderRadius: BorderRadius.all(Radius.circular(35)),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            // mainAxisSize: MainAxisSize.min,
                            children: [
                              // Padding(
                              //   padding: EdgeInsets.only(bottom: height * 0.008),
                              // ),
                              Text(
                                  "${formatter3.format(forecastWeather[index].date!)}"),
                              // Padding(
                              //   padding: EdgeInsets.only(bottom: height * 0.008),
                              // ),
                              Text(
                                  "${formatter2.format(forecastWeather[index].date!)}"),
                              // Padding(
                              //   padding: EdgeInsets.only(bottom: height * 0.007),
                              // ),
                              Image.network(
                                  "https://openweathermap.org/img/wn/${forecastWeather[index].weatherIcon}.png"),
                              // Padding(
                              //   padding: EdgeInsets.only(bottom: height * 0.02),
                              // ),
                              Text("${forecastWeather[index].weatherMain}"),
                              // Padding(
                              //   padding: EdgeInsets.only(bottom: height * 0.02),
                              // ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                      "${forecastWeather[index].tempMax!.celsius!.round().toString()}/${forecastWeather[index].tempMin!.celsius!.round().toString()}"),
                                  EasyRichText(
                                    " \u00B0" + "C",
                                    patternList: [
                                      EasyRichTextPattern(
                                        targetString: "C",
                                        superScript: true,
                                        stringBeforeTarget: ' \u00B0',
                                        matchWordBoundaries: false,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              Text("Wind Speed"),
                              Text("${forecastWeather[index].windSpeed}" +
                                  " m/sec"),
                              Text("${forecastWeather[index].windDegree}" +
                                  " Deg"),
                              Text("${forecastWeather[index].pressure}" +
                                  " hPa"),
                              Text("${forecastWeather[index].humidity}" + " %"),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
